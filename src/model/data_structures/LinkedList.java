package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>>  
{
	/**
	 * Agrega un nuevo elemento a la lista
	 * @param element: Elemento que se quiere agregar a la lista
	 * @return true si se pudo agregar el elemento false de lo contrario
	 */
	public boolean add(T element);
	
	/**
	 * Elimina un elemento a la lista
	 * @param element: Elemento que se quiere eliminar a la lista
	 * @return true si se pudo eliminar el elemento false de lo contrario
	 */
	public boolean delete(T element);
	
	/**
	 * 
	 * @param element
	 * @return Un elemento de la lista 
	 */
	public Node get(T element);
	
	/**
	 * Dar el tama�o de la lista
	 * @return El tama�o de la lista
	 */
	public int size();
	
	/**
	 * Dar un elemento seg�n la posici�n que ocupa en la lista
	 * @param position: Posici�n que ocupa el elemento en la lista
	 * @return El elemento en la posici�n indicada
	 */
	public Node get(int position);
	
	/**
	 * 
	 */
	public T[] listing(T[] arreglo);
	
	/**
	 * 
	 * @return
	 */
	public Node getCurrent();
	
	/**
	 * 
	 * @param element
	 * @return
	 */
	public boolean next(T element);

	

}
