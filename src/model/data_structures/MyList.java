package model.data_structures;

public class MyList <T extends Comparable<T>> implements LinkedList<T>
{
	// Atributos
	private Node first;

	private Node current;

	private Node last;

	private int size;

	//M�todos
	public MyList()
	{
		first = null;
		current = null;
		last = null;
		size = 0;
	}


	@Override
	public boolean add(T element) 
	{
		boolean agrego= false;
		/**No hay elementos en la lista -> se agrega como primero**/
		if( first == null)
		{
			first = new Node(element,size);
			current = first;
			last = first;
			size++;
			agrego= true;
		}
		/**Ya existen elementos en la lista-> se agrega al final **/
		else 
		{
			Node agregar = new Node(element,size);
			last.changeNext(agregar);
			agregar.changeBefore(last);
			last = agregar;
			current = last;
			size++;
		}

		return agrego;
	}


	@Override
	public boolean delete(T element) {
		boolean elimino= false;
		Node eliminar = get(element);
		/**Existe el nodo a aliminar**/
		if(eliminar!=null)
		{
			/**Es el primero de la lista **/
			if(first.compareTo(eliminar))
			{
				Node newFirst = first.getNext();
				first.changeNext(null);
				newFirst.changeBefore(null);
				elimino = true;
			}

			/**Es el �ltimo de la lista **/
			else if(last.compareTo(eliminar))
			{
				Node newLast = last.getBefore();
				last.changeBefore(null);
				newLast.changeNext(null);
				last=newLast;
				elimino = true;
			}

			/**Est� en el medio*/
			else 
			{
				Node actual = first;
				while(actual.hasNext() && !elimino)
				{
					if(actual.compareTo(eliminar))
					{
						Node antes=actual.getBefore();
						Node luego=actual.getNext();
						antes.changeNext(luego);
						luego.changeBefore(antes);
						actual.changeBefore(null);actual.changeNext(null);
						elimino = true;
					}
					actual = actual.getNext();
				}
			}
		}

		return elimino;
	}



	@Override
	public Node get(T element) {
		Node buscado = null;
		if(first!=null)
		{
			Node actual = first;
			if(actual.getMe().compareTo((Node) element))
			{
				buscado = actual;
			}
			else
			{
				while(actual.hasNext() && buscado==null)
				{
					actual = actual.getNext();
					if(actual.getMe().compareTo((Node) element))
						buscado = actual;
				}
			}
		}
		return buscado;
	}

	@Override
	public Node get(int position) 
	{
		Node buscado = null;
		if(first!=null)
		{
			Node actual = first;
			if(actual.getPos()==position)
			{
				buscado = actual;
			}
			else
			{
				while(actual.hasNext() && buscado==null)
				{
					actual = actual.getNext();
					if(actual.getPos()==position)
						buscado = actual;
				}
			}
		}
		return buscado;
	}


	@Override
	public int size() 
	{
		return size;
	}


	@Override
	public T[] listing(T[] arr) {
		Node<T> actual = first;
		int cuantos = 0;
		if(actual!=null)
		{
			arr[cuantos]=(T) actual.getMe();
			cuantos++;
		}		
		else
		{
			while(actual.hasNext())
			{
				actual = actual.getNext();
				arr[cuantos]=(T) actual.getMe();
				cuantos++;
			}
		}
		return arr;
	}


	@Override
	public Node getCurrent() 
	{
		return current;
	}


	@Override
	public boolean next(T element) 
	{
		boolean avanzo=false;
		current=current.getNext();
		return avanzo;
	}
	
	public Node getFirst()
	{
		return first;
	}
	
	public Node getLast()
	{
		return last;
	}


}
