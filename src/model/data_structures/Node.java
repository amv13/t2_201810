package model.data_structures;

public class Node <T extends Comparable<T>>
{
	// Atributos
	/**
	 * Nodo anterior a this
	 */
	private Node before;
	
	/**
	 * Nodo siguiente a this
	 */
	private Node next;
	
	/**
	 * Posici�n del nodo en la lista
	 */
	private int pos;
	
	// M�todos
	public <T> Node (T element, int posicion) 
	{
		before = null;
		next = null;
		pos = posicion;
	}
	
	public Node getBefore()
	{
		return before;
	}
	
	public void changeBefore(Node element)
	{
		before = element;
	}
	
	public Node getNext()
	{
		return next;
	}
	
	public void changeNext(Node element)
	{
		next = element;
	}
	
	public Node getMe()
	{
		return this;
	}
	
	public boolean hasNext()
	{
		if(next!=null)
			return true;
		else
			return false;
	}
	
	public boolean compareTo(Node eliminar)
	{
		boolean igual = false;
		if(this.equals(eliminar))
			igual=true;
		
		return igual;
	}
	
	public int getPos()
	{
		return pos;
	}
	
	
}
