package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.MyList;

public class TaxiTripsManager implements ITaxiTripsManager {

	private MyList taxis;
	
	private MyList servicios;
	
	// TODO
	// Definition of data model 

	public void loadServices (String serviceFile) {
		System.out.println("Inside loadServices with " + serviceFile);
		JsonParser parser = new JsonParser();
		
		servicios = new MyList<Service>();
		taxis = new MyList<Taxi>();

		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				
				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");
				//System.out.println(obj);
				
				/* Obtener la propiedad trip_id de un servicio (String)*/
				String trip_id = "NaN";
				if ( obj.get("trip_id") != null )
				{ trip_id = obj.get("trip_id").getAsString(); }
				
				/* Obtener la propiedad taxi_id de un servicio (String)*/
				String taxi_id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ trip_id = obj.get("taxi_id").getAsString(); }
				
				/* Obtener la propiedad company de un servicio (String)*/
				String company = "NaN";
				if ( obj.get("company") != null )
				{ company = obj.get("company").getAsString(); }
				else
				{ company = null;}
				
				/* Obtener la propiedad trip_seconds de un servicio (Int)*/
				int trip_seconds = -1;
				if ( obj.get("trip_seconds") != null )
				{ trip_seconds = obj.get("trip_seconds").getAsInt(); }
				
				/* Obtener la propiedad trip_miles de un servicio (Double)*/
				double trip_miles = -1;
				if ( obj.get("trip_miles") != null )
				{ trip_miles = obj.get("trip_miles").getAsDouble(); }
				
				/* Obtener la propiedad trip_total de un servicio (Double)*/
				double trip_total = -1;
				if ( obj.get("trip_total") != null )
				{ trip_total = obj.get("trip_total").getAsDouble(); }
				
				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				JsonObject dropoff_localization_obj = null; 

				//System.out.println("(Lon= "+dropoff_centroid_longitude+ ", Lat= "+dropoff_centroid_latitude +") (Datos String)" );

				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();
				//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");
				
				//Crear un servicio
				Service actualS=new Service(trip_id, taxi_id, trip_seconds, trip_miles, trip_total);
				servicios.add(actualS);
				Taxi actualT= new Taxi(taxi_id, company);
				taxis.add(actualT);
				}
				else
				{
					System.out.println( "[Lon: NaN, Lat: NaN ]");
				}
			}
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}


	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		System.out.println("Inside getTaxisOfCompany with " + company);
		MyList<Taxi> taxisCompany = new MyList<Taxi>();
		if(taxis.getFirst().getCompany().equals(company))
		{
			taxisCompany.add(taxis.getFirst());
		}
		Taxi actual = taxis.getFirst();
		while(actual.hasNext())
		{
			if(actual.getCompany().equals(company))
			{
				taxisCompany.add(actual);
			}
			actual = actual.getNext;
		}
		
		return taxisCompany;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		MyList<Taxi> taxisCommunity = new MyList<Taxi>();
		if(taxis.getFirst().getComunityArea().equals(communityArea))
		{
			taxisCommunity.add(taxis.getFirst());
		}
		Taxi actual = taxis.getFirst();
		while(actual.hasNext())
		{
			if(actual.getCompany().equals(communityArea))
			{
				taxisCommunity.add(actual);
			}
			actual = actual.getNext;
		}
		
		return taxisCommunity;

	}


}
